let author = document.querySelector('footer');

author.addEventListener('click', updateName);

function updateName() {
  let name = prompt('Enter a new name');
  author.innerHTML = '&copy;' + name;
}